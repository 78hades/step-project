let nameServices = document.querySelector(`#section-name-services`);
let nameSection;
let firstTab = document.querySelector(`#tab-active`);
let firstBlock = document.querySelector(`#content-active`);
firstBlock.style.display = `flex`;
firstTab.style.backgroundColor = `rgba(24, 207, 171, 1)`;
firstTab.classList.add(`pseudo-element-name-service-fist`);
nameServices.onclick = function (event) {
  firstBlock.style.display = `none`;
  firstTab.style.backgroundColor = `rgba(248, 252, 254, 1)`;
  firstTab.classList.remove(`pseudo-element-name-service-fist`);
  let section = event.target.closest(`li`);
  if (!section) return;
  if (!nameServices.contains(section)) return;
  clickSection(section);
};
function clickSection(li) {
  if (nameSection) {
    nameSection.classList.remove(`pseudo-element-name-service`);
    nameSection.style.backgroundColor = ` rgba(248, 252, 254, 1)`;
    getSelectedBlock = document.querySelector(`[data-content = ${getKeyWord}]`);
    getSelectedBlock.style.display = "none";
  }
  nameSection = li;
  getKeyWord = nameSection.getAttribute(`data-name`);
  nameSection.classList.add(`pseudo-element-name-service`);
  nameSection.style.backgroundColor = `rgba(24, 207, 171, 1)`;
  getSelectedBlock = document.querySelector(`[data-content = ${getKeyWord}]`);
  getSelectedBlock.style.display = "flex";
}

const DivWithImages = [
  {
    link: "img/graphic design-min.webp",
    category: "Graphic Design",
  },
  { link: "img/1_aTYOTFS4Vkr-nwHNML3GvQ.jpg", category: "Landing Pages" },
  {
    link: "img/What-is-Your-Website-Design-Process-Blog-Post-Feature-Image-36119-01.png",
    category: "Web Design",
  },
  {
    link: "img/wordpress-sitemaps.svg",
    category: "Wordpress",
  },
  {
    link: "img/graphic.jpeg",
    category: "Graphic Design",
  },
  { link: "img/landing-page-examples-1.webp", category: "Landing Pages" },
  {
    link: "img/web-design.img4.jpeg",
    category: "Web Design",
  },
  {
    link: "img/wordpress.jpg",
    category: "Wordpress",
  },
  {
    link: "img/1620032973587.png",
    category: "Graphic Design",
  },
  {
    link: "img/319d0c73c7906bfeb421312b48546c0fd83f33f9-2400x1256.jpg",
    category: "Landing Pages",
  },
  {
    link: "img/CT9xCjqrhnPD4ivB6B8Hqe.jpg",
    category: "Web Design",
  },
  {
    link: "img/blog5feae4230520c_wordpres3s.png",
    category: "Wordpress",
  },
  {
    link: "img/photo-1626785774573-4b799315345d.jpeg",
    category: "Graphic Design",
  },
  {
    link: "img/media_1989cc7d902aa64ab900604ff44f877f8eae74ac4.png",
    category: "Landing Pages",
  },
  {
    link: "img/4a5235_43a477c4822947a5bdebaf6247304813~mv2.png",
    category: "Web Design",
  },
  {
    link: "img/wordpress-website.webp",
    category: "Wordpress",
  },
  {
    link: "img/blog-lydia-easun.jpg",
    category: "Graphic Design",
  },
  { link: "img/landing-page-img.png", category: "Landing Pages" },
  {
    link: "img/which-web-design-platform-is-right-for-you-5e4d59fb941b9.webp",
    category: "Web Design",
  },
  {
    link: "img/img.wordpress.jpeg",
    category: "Wordpress",
  },
  {
    link: "img/3848e9c61bc48c126617aa5d6c81a999.jpg",
    category: "Graphic Design",
  },
  {
    link: "img/bigstock-Website-Landing-Page-Developm-229946338.08dbd768.jpg",
    category: "Landing Pages",
  },
  {
    link: "img/6410ebf8e483b59fa986fb66_ABM college web design skills main.jpg",
    category: "Web Design",
  },
  {
    link: "img/wordpress-cookies.png",
    category: "Wordpress",
  },
];
let newImages = document.querySelector(`.images-of-work`);
DivWithImages.forEach((img) => {
  let div = document.createElement("div");
  div.classList.add("div-of-work");
  div.style.display = "none";
  div.setAttribute("data-content-second-section", `${img.category}`);
  let image = document.createElement("img");
  image.classList.add("img-of-work");
  image.src = `${img.link}`;
  div.append(image);
  newImages.append(div);
});

let buttonClick = document.querySelector("#load");
let namesOfWork = document.querySelector("#names-of-work");
let allImages = document.querySelectorAll(".images-of-work div");
let circleLoaderFirst = document.querySelector("#pl-first");
let displayDuration = 2000;
let light = true;
let choosenSection = true;
let SecondClick;
namesOfWork.onclick = function (event) {
  SecondClick = true;
  let section = event.target.closest("li");
  if (!section) return;
  let selectedSection = section.getAttribute("data-name-second-section");
  choosenSection = false;
  document.querySelectorAll(`.div-second-first-section`).forEach((img) => {
    let dataContent = img.getAttribute("data-content-second-section");
    if (selectedSection === "all" || selectedSection === dataContent) {
      img.style.display = "flex";
    }
  });

  allImages.forEach((img, index) => {
    let dataContent = img.getAttribute("data-content-second-section");
    if (choosenSection === false) {
      if (index >= 12 && index < 24) {
        if (img.style.display === `flex`)
          if (selectedSection === "all" || selectedSection === dataContent) {
            buttonClick.style.display = `flex`;
            SecondClick = false;
          }
      }
    }
    if (choosenSection === false) {
      if (index >= 24 && index < 36) {
        if (img.style.display === `flex`)
          if (selectedSection === "all" || selectedSection === dataContent) {
            buttonClick.style.display = `none`;
          } else {
            buttonClick.style.display = `flex`;
          }
      }
    }
  });

  buttonClick.onclick = function () {
    buttonClick.style.display = "none";
    circleLoaderFirst.style.display = "block";

    allImages.forEach((img, index) => {
      let attributeOfDiv = img.getAttribute(`data-content-second-section`);
      setTimeout(function () {
        circleLoaderFirst.style.display = "none";
        if (
          index >= 12 &&
          index < 24 &&
          (selectedSection === "all" || selectedSection === `${attributeOfDiv}`)
        ) {
          img.style.display = "flex";
          buttonClick.style.display = `flex`;
        }
      }, displayDuration);

      if (SecondClick === false) {
        setTimeout(function () {
          circleLoaderFirst.style.display = "none";
          if (
            index >= 24 &&
            index < 36 &&
            (selectedSection === "all" ||
              selectedSection === `${attributeOfDiv}`)
          ) {
            img.style.display = "flex";
            buttonClick.style.display = `none`;
          }
        }, displayDuration);
      }
    });

    SecondClick = false;
  };
};

buttonClick.onclick = function () {
  buttonClick.style.display = "none";
  circleLoaderFirst.style.display = "block";
  setTimeout(function () {
    circleLoaderFirst.style.display = "none";
    if (choosenSection === true) {
      allImages.forEach((img, index) => {
        if (index >= 12 && index < 24) {
          img.style.display = "flex";
          buttonClick.style.display = "flex";
        }
      });
    }
  }, displayDuration);
  setTimeout(function () {
    if (!SecondClick) {
      circleLoaderFirst.style.display = "block";
      allImages.forEach((img, index) => {
        if (!SecondClick && index >= 24 && index < 36) {
          img.style.display = "flex";
          buttonClick.style.display = "none";
          circleLoaderFirst.style.display = "none";
        }
      });
    }
  }, displayDuration);

  SecondClick = !SecondClick;
};

allImages.forEach((img) => {
  let getKeyWord = img.getAttribute("data-content-second-section");
  img.classList.add(`div-of-work`);
  let hoverDiv = document.createElement(`div`);
  img.appendChild(hoverDiv);
  hoverDiv.classList.add("hover-div");
  let imgOneFirstPart = document.createElement(`img`);
  hoverDiv.appendChild(imgOneFirstPart);
  imgOneFirstPart.src = `img/Ellipse 1 copy 3.png`;
  let imgOneSecondPart = document.createElement(`img`);
  hoverDiv.appendChild(imgOneSecondPart);
  imgOneSecondPart.src = `img/Combined shape 7431.png`;
  let imgTwoFirstPart = document.createElement("img");
  hoverDiv.appendChild(imgTwoFirstPart);
  imgTwoFirstPart.src = `img/Ellipse 1.png`;
  let imgTwoSecondPart = document.createElement("img");
  hoverDiv.appendChild(imgTwoSecondPart);
  imgTwoSecondPart.src = `img/Layer 23.png`;
  let titleFirst = document.createElement(`h2`);
  hoverDiv.appendChild(titleFirst);
  titleFirst.textContent = `creative design`;
  let secondTitle = document.createElement(`h3`);
  hoverDiv.appendChild(secondTitle);
  secondTitle.textContent = `${getKeyWord}`;
});
const clients = [
  {
    feedback: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem,
non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam
facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum
odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.`,
    name: `hasan ali`,
    jobTitle: `UX Designer`,
  },
  {
    feedback: `Dignissimos, error laborum unde perspiciatis iusto aliquam quo.Qui
exercitationem voluptatibus cumque, atque quas, reiciendis illo eius
suscipit animi iusto veritatis odit nulla vitae corporis officia fugiat
sint sunt dicta, labore est iste sit facilis. Nostrum quam labore beatae
ipsa.`,
    name: `Natali Beluchi`,
    jobTitle: `Front-end Developer`,
  },
 { feedback: `Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis
 nisi quo laboriosam fugit id eum debitis natus. Praesentium voluptatem
 sapiente laboriosam quis minus serqui vel repellendus laborum, veniam
 illum quas odit culpa facilis explicabo similique pariatur!`,
      name: `Mathew Macconery`,
      jobTitle: `Game Designer`,
    },
    { feedback: `Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis
    nisi quo laboriosam fugit id eum debitis natus. Praesentium voluptatem
    sapiente laboriosam quis minus serqui vel repellendus laborum, veniam
    illum quas odit culpa facilis explicabo similique pariatur!`,
         name: `Kristina Portman`,
         jobTitle: `Game Developer`,
       },
];
let avatarClients = document.querySelectorAll(`.avatars-clients`);
let buttonBack = document.querySelector(`#back`);
let buttonNext = document.querySelector(`#next`);
let feedbackFromClient = document.querySelector(`#feedback`);
let avatarClient = document.querySelector(`#avatar-client`);
let jobTitle = document.querySelector(`#job-title`);
let nameClient = document.querySelector(`#name-client`);
let index = 0;
function switchClients() {
  const currentClient = clients[index];
avatarClients.forEach((client, i) => {
    client.style.marginTop = i === index ? "-40px" : "-20px";
    client.classList.toggle("active", i === index);
  });
  avatarClient.src = avatarClients[index].getAttribute("src");
  feedbackFromClient.textContent = currentClient.feedback;
  jobTitle.textContent = currentClient.jobTitle;
  nameClient.textContent = currentClient.name;
}
avatarClients.forEach((client, i) => {
  client.addEventListener('click', function() {
    index = i;
    switchClients();
  });
});
buttonNext.onclick = function () {
  index = (index + 1) % avatarClients.length;
  switchClients();
};

buttonBack.onclick = function () {
  index = (index - 1 + avatarClients.length) % avatarClients.length;
  switchClients();
};
const newPicsinGallery = [
  { link: "img/ringve-museum-1 1.png" },
  { link: "img/vanglo-house-1 2.png" },
  { link: "img/ringve-museum-1 2.png" },
  { link: "img/Kids-Store-Lighting-2 1.png" },
  { link: "img/ia-Pools-1 1.png" },
  { link: "img/Kids-Store-Lighting-1 (1) 1.png" },
  { link: "img/vanglo-house-6 2.png" },
  { link: "img/Kids-Store-Lighting-2 2 (1).png" },
  { link: "img/vanglo-house-4 1.png" },
  { link: "img/IA-Hourses-6 1.png" },
  { link: "img/vanglo-house-1 3.png" },
  { link: "img/IA-billionares-6 1.png" },
  { link: "img/Brazil-staduims-IA-1 1.png" },
  { link: "img/7328272788_c5048326de_o 1.png" },
  { link: "img/p1_8 1.png" },
  { link: "img/vanglo-house-6 1.png" },
  { link: "img/80493541_1281644acb_o 1.png" },
  { link: "img/vanglo-house-1 1.png" },
];

let gallery = document.querySelector(`#gallery`);
let divInGallery = document.createElement(`div`);
divInGallery.classList.add(`img-of-gallery`);
divInGallery.style.display = `none`;
gallery.append(divInGallery);
newPicsinGallery.forEach((img, index) => {
  if (index < 3 || index > 13) {
    let image = document.createElement(`img`);
    image.classList.add(`img-of-gallery`);
    image.src = `${img.link}`;
    image.style.display = `none`;
    gallery.appendChild(image);
  }
  if (index > 1 && index < 13) {
    let imageInserted = document.createElement(`img`);
    imageInserted.src = `${img.link}`;
    imageInserted.classList.add(`inserted`);
    divInGallery.appendChild(imageInserted);
  }
});
document.addEventListener("DOMContentLoaded", function () {
  function masonry() {
    let mas;
    function initializeMasonry() {
      mas = new Masonry(gallery, {
        itemSelector: ".img-of-gallery",
        columnWidth: 378,
        gutter: 13,
      });
    }
    initializeMasonry();

    let secondLoadButton = document.querySelector("#second-load");
    let imgsOfGallery = document.querySelectorAll(".img-of-gallery");

    secondLoadButton.onclick = function () {
      secondLoadButton.style.display = "none";
      imgsOfGallery.forEach((img, index) => {
        setTimeout(function () {
          if (index >= 8 && index < 16) {
            img.style.display = "inline-block";
          }
          initializeMasonry();
          gallery.style.marginBottom = "80px";
        }, displayDuration);
      });
    };
  }
  masonry();
});
